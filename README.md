daisy-creator-magazin
=====================

Create digital talking books of audio magazins for blind people in the daisy 2.02 standard.

![Screenshot DaisyCreator Main Page](images/Screenshot_DaisyCreator_Main.png)

Basics
------
The workflow is as follows:
- Record your articles with a digital recorder, like the marantz PMD 661 or an DAW
- The produced audiofiles with PMD 661 are stored as mp3 in a flat struckture with numbered filenames up to 1000. Or, with a DAW save your files acording this syntax: 1000_Title_first.mp3
- Now start the daisy-creator-magazin-application to create the daisy-fileset

If finished, you will find in the destination folder your audiofiles
and the daisy-structure for your daisy-player.
The daisy fileset is build from your filenames and your meta data.

General work steps
------------------
1. Choose the folder of your recordet audiofiles as "Source-Folder"
2. Choose the folder for your dtb as "Destination-Folder"
3. Choose the title and issue number of your magazin
4. Select the options if necessary (see options-section)
5. Klick "Next" (Weiter)
6. Run "Copy" (see the log on Tab 2)
7. Klick "Next" (Weiter)
8. Control and edit your metadata if necessary
9. Klick "Next" (Weiter)
10. Run "Daisy"
9. Klick "Next" (Weiter)
10. Run "Validate"
Done!

Additional audiofiles-section
-----------------------------
With this, you can load and insert in the daisystruckture one or two separate recordet audiofiles,
they not are on your mediacard. To sort the audiofiles before your audios from the mediacard,
lets the filename begin with the number "0101".

Options-Section on tab one
--------------------------
Here, you can select:
1. Copy Magazin-Issue
This will copy a audiofile, with e.g. a separate produced announcement of your issue-number, in the daisy-structure
2. Copy Intro
This will copy a audiofile with a separate produced jingle or intro
3. Copy Imprint
This will copy a audiofile with a separate produced imprint
3. Change bitrate
This checks the bitrate of each audiofile and changes it to the choosen rate on the "Preferences-Tab"
4. Rename 1000 (or 1001) to 0100 (0101)
This is used, when the first recordet audiofile (e.g. with a special announcement)
must sorted before the additive audiofiles (see Additional audiofiles)
5. For DTB TOC: Use titles from filenames and cut leading numbers in filename for TOC
6. Read levels from filenames (digit after first underscore: 1001_2)
7. Read date from filenames for DTB TOC
8. Chose character for split author and title for DTB TOC
9. Bitrate

The follwoing options will be set by some tests while choose the source folder and the source file additionaly:

* Bitrate will be set to the most used by the source mp3 files
* Rename 1000 (or 1001) to 0100 (0101) will be set by select the first source file additionaly
* Use titles from filenames for TOC will be set if the source mp3 filenames contain more then the four digits

Preferences
-----------

- The titles and available numbers of your magazins will be load from the configfile: daisy_creator_mag.config.
See daisy_creator_mag.config.sample for the syntax.
- The meta data for each magazin will be load from a file in the folder (set in config-file) by the name of the magazin.
See Daisy_Meta_My_Magazin.sample for the syntax

Config
------

On the config tab you can

* for example edit your working paths
* and, if you organize your files in folders, one for every year,
  make the folder struckture for the new year

To create a new working year and folders

* click the button "Neues Arbeitsjahr anlegen"
* it takes the new year from the current an adds one
* and replaces all old years in the paths wit the new ones

After that, klick the button "Speichern" to save the new config

To create a new config edit the entrys for your needs:

* The first entrys contains the working paths and folders
* The "Jahr-Metadata" is for the year, that is predefined in the meta files of each Magazin
* The Präfix items contains the prefixes for mata and issue nr files
* BHZ-Liste defines the items of magazins, listet in the drop downbox on tab 1
* Daisy-Pipeline: Package, Script, Options, Path to validate report

If are all settings done, press "Speichern", the following tasks will be done:

* The old config file is backed up
* New working paths and folders are created
* Intro files are copied form old to new destination
* Imprint files are copied form old to new destination
* Meta files are created from old ones and updatet to the new year

Remark
------
The daisy_creator_mag is written by Joerg Sorge for KOM-IN-Netzwerk e.V. www.kom-in.de

Credits
-------
Icons from https://icons8.com

Installation
------------

The DaisyCreator is testet on ubuntu 18 and 20

- Install common dependencies

  * `sudo apt install python3-pyqt5 python3-mutagen python3-html2text lame`

- Install pyqtdarktheme via PyPi:

  * `pip install pyqtdarktheme`

- Install DaisyPipeline2

  * Prepearing for installing Java

    * `sudo apt-get install wget apt-transport-https gnupg`
    * `wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | sudo apt-key add -`
    * `sudo echo "deb https://packages.adoptium.net/artifactory/deb $(awk -F= '/^VERSION_CODENAME/{print$2}' /etc/os-release) main" | sudo tee /etc/apt/sources.list.d/adoptium.list`
    * `sudo apt update`

  * `sudo apt install temurin-11-jdk`

  * Download DaisyPipeline2 from repository:
    https://daisy.github.io/pipeline/Download.html#latest-version

    `pipeline2-1.14.4_debian.deb`

  * Install `sudo dpkg -i pipeline2-1.14.4_debian.deb`

  * Download DaisyPipeline2 CommandLineTool from repository:
    https://github.com/daisy/pipeline-cli-go/releases

  * Install `sudo dpkg -i cli-2.1.1-linux_386.deb`

- Install DaisyCreator

* Clone the repo

  * `git clone https://codeberg.org/cExplorer/daisy-creator-magazin.git`


* In the directory `daisy-creator-magazin`:

  * Copy the file `daisy_creator_mag.config.sample` to `daisy_creator_mag.config`
  * Edit the file for your needs for example with `nano`

    `nano daisy_creator_mag.config`

- Install the Desktop-Launcher for the local user

  In the directory `daisy-creator-magazin`:

  * Copy the file `daisy_creator_mag_py.desktop.sample` to `daisy_creator_mag_py.desktop`

    `cp daisy_creator_mag_py.desktop.sample daisy_creator_mag_py.desktop`
  * Edit the paths for `Icon`, `Path` and `Exec` in the file for your needs for example with `nano`

    `nano daisy_creator_mag_py.desktop`
  * Copy the file to the user home `.local/share/applications`

    `cp daisy_creator_mag_py.desktop ~/.local/share/applications`
  * To add DaisyCreator to the favorities under Gnome 3,
    launch the `Activities`, right-click on the DaisyCreator Icon, choose `Add to Favorities`
