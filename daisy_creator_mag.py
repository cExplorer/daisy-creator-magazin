#!/usr/bin/python3
# -*- coding: utf-8 -*-
# pylint: disable-msg=C0103

"""
Autor: Joerg Sorge
Distributed under the terms of GNU GPL version 2 or later
Copyright (C) Joerg Sorge joergsorge at googel
2012-06-20

Dieses Programm
- kopiert mp3-Files fuer die Verarbeitung zu Daisy-Buechern
- erzeugt die noetigen Dateien fuer eine Daisy-Struktur.

This program
- makes copys of mp3-files from audiorecordings for daisy-production
- builds the daisy-struckture for digital audio books in daisy 2.02 standard

needs:
python3-pyqt5
pyqtdarktheme
python3-mutagen
python3-html2text
lame

sudo apt install python3-pyqt5 python3-mutagen python3-html2text lame

installing pyqtdarktheme via pypi:
pip install pyqtdarktheme

Create the ui.py file:
pyuic5 daisy_creator_mag.ui > daisy_creator_mag_ui.py

update qt-GUI by development:
pyuic5 daisy_creator_mag.ui -o daisy_creator_mag_ui.py

code-checking with:
pylint --disable=W0402 --const-rgx='[a-z_A-Z][a-z0-9_]{2,30}$'
daisy_creator_mag.py
"""

import sys
import os
import shutil
import datetime
from datetime import timedelta
import apt
import re
import operator
import importlib.util
import html2text
import configparser
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtGui import QMovie
from mutagen.mp3 import MP3
from mutagen.id3 import ID3
from mutagen.id3 import ID3NoHeaderError
from time import sleep
import daisy_creator_mag_ui


class Worker(QtCore.QObject):
    finished = QtCore.pyqtSignal()
    progress = QtCore.pyqtSignal(str)
    packages_to_check = []

    def run_packages_to_check(self):
        """Long-running task."""
        # this sleep prevent the gui from freezing
        # and let then run the next code in background
        sleep(0.5)
        self.progress.emit("<b>Installationspruefung:</b>")
        cache = apt.Cache()
        cache.open()
        for item in self.packages_to_check:
            try:
                cache[item].is_installed
                self.progress.emit(item)
            except KeyError:
                error_message = (
                    "Es fehlt das Paket:<br> " + item
                    + "<br>Zur Nutzung des vollen Funktionsumfanges "
                    + "muss es installiert werden!")
                self.progress.emit(error_message)


class TaskManager(QtCore.QObject):
    started = QtCore.pyqtSignal()
    finished = QtCore.pyqtSignal()
    progressChanged = QtCore.pyqtSignal(int, QtCore.QByteArray)

    def __init__(self, parent=None):
        QtCore.QObject.__init__(self, parent)
        self._process = QtCore.QProcess(self)
        self._process.finished.connect(self.handleFinished)
        self._progress = 0

    def start_tasks(self, tasks):
        self._tasks = iter(tasks)
        self.fetchNext()
        self.started.emit()
        self._progress = 0

    def fetchNext(self):
        try:
            task = next(self._tasks)
        except StopIteration:
            return False
        else:
            self._process.start(*task)
        return True

    def processCurrentTask(self):
        output = self._process.readAllStandardOutput()
        self._progress += 1
        self.progressChanged.emit(self._progress, output)

    def handleFinished(self):
        self.processCurrentTask()
        if not self.fetchNext():
            self.finished.emit()


class DaisyCopy(QtWidgets.QMainWindow, daisy_creator_mag_ui.Ui_DaisyMain):
    """
    mainClass
    The second parent must be 'Ui_<obj. name of main widget class>'.
    """

    process_validator_error_signal = QtCore.pyqtSignal(str)
    process_validator_output_signal = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        """Settings"""
        super(DaisyCopy, self).__init__(parent)
        # This is because Python does not automatically
        # call the parent's constructor.
        self.setupUi(self)
        # Pass this "self" for building widgets and
        # keeping a reference.
        self.app_debug_mod = "yes"
        self.app_mag_items = ["Zeitschrift"]
        self.app_issue_items_prev = [
            "06", "07", "08", "09", "10", "11", "12", "21", "22", "23", "24"]
        self.app_issue_items_current = [
            "I", "II", "III", "IV", "01", "02",
            "03", "04", "05", "06", "07", "08", "09",
            "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22", "23", "24"]
        self.app_issue_items_next = ["I", "II", "01", "02", "03"]
        self.app_mag_path = QtCore.QDir.homePath()
        self.app_mag_path_dest = QtCore.QDir.homePath()
        self.app_mag_path_meta = QtCore.QDir.homePath()
        self.app_mag_path_issue_nr = QtCore.QDir.homePath()
        self.app_mag_path_intro = QtCore.QDir.homePath()
        self.app_mag_path_imprint = QtCore.QDir.homePath()
        self.app_mag_path_add_file = QtCore.QDir.homePath()
        self.app_mag_prefix_issue_nr = ""
        self.app_mag_prefix_meta = ""
        self.app_orig_source_files = []

        # for titles from filenames
        self.list_titles_from_files = []
        # we need ext package lame and daisy pipeline
        self.app_validator = ""
        self.app_validator_script = ""
        self.app_validator_time = ""
        self.app_validator_path = ""

        # for mp3 files needed to recode
        self.dict_files_to_recode = {}

        # for processes validator
        # self.process_output = None
        # self.process_error = None
        self.process_validator = QtCore.QProcess()
        self.process_validator.readyReadStandardError.connect(
            self.process_validator_on_ready_read_standard_error)
        self.process_validator.readyReadStandardOutput.connect(
            self.process_validator_on_ready_read_standard_output)

        # for process lame
        self.manager_bitrate = TaskManager(self)
        self.manager_bitrate.progressChanged.connect(
            self.process_bitrate_data_ready)

        # for find text in textEdit
        self.textDoc = QtGui.QTextDocument()
        # for animated gif
        self.movie = QMovie("cube-gif-6_cropped.gif")
        self.labelWaitBitrate.setMovie(self.movie)
        self.labelWaitValidate.setMovie(self.movie)
        self.connect_actions()

    def process_bitrate_data_ready(self, progress, result):
        """process bitrate data ready"""
        self.textEditConsole.append(str(result, "utf-8"))
        self.progressBarCopy.setValue(progress)

    def process_bitrate_finished(self):
        """process bitrate finished"""
        self.textEdit.append("<b>Bitrate aendern abgeschlossen</b><br>")
        self.commandLinkButton.setDisabled(False)
        self.commandLinkButtonDaisy.setDisabled(False)
        self.commandLinkButtonValidate.setDisabled(False)
        self.commandLinkButton.setStyleSheet(
            "background-color: yellow")
        self.movie.stop()
        self.labelWaitBitrate.hide()
        self.check_change_id3_preparation()

    def process_validator_on_ready_read_standard_error(self):
        """process validator on ready read standard error"""
        process_error = (
            self.process_validator.readAllStandardError().data().decode())
        self.textEditConsole.append(process_error)
        self.process_validator_error_signal.emit(process_error)

    def process_validator_on_ready_read_standard_output(self):
        """process validator on ready read standard output"""
        result = self.process_validator.readAllStandardOutput().data().decode()
        self.textEditConsole.append(result)
        self.process_validator_output_signal.emit(result)

    def process_validator_finished(self):
        """process validator finished"""
        self.textEditValidate.append("Validierung abgeschlossen")
        # search for SUCCESS
        # the cursor in self.textEditValidate is on the end of text,
        # so we must search backwards
        flag = self.textDoc.FindBackward
        success = self.textEditConsole.find('SUCCESS', flag)
        self.show_debug_message(success)

        if not success:
            self.commandLinkButtonValidate.setStyleSheet(
                "background-color: yellow")
            self.validate_daisy_load_report()
        else:
            log_message = "Daisy-Validierung erfolgreich!"
            self.show_debug_message(log_message)
            self.textEditValidate.append(
                "<b>Daisy-Validierung erfolgreich!</b>")
            self.commandLinkButtonValidate.setStyleSheet(
                "background-color: green")

        self.commandLinkButton.setDisabled(False)
        self.commandLinkButtonDaisy.setDisabled(False)
        self.commandLinkButtonValidate.setDisabled(False)
        self.movie.stop()
        self.labelWaitValidate.hide()

    def connect_actions(self):
        """define Actions """
        self.toolButtonCopySource.clicked.connect(self.action_open_copy_source)
        self.toolButtonCopyDest.clicked.connect(self.action_open_copy_dest)
        self.toolButtonCopyFile1.clicked.connect(self.action_open_copy_file_1)
        self.toolButtonCopyFile2.clicked.connect(self.action_open_copy_file_2)
        self.commandLinkButton.clicked.connect(self.action_run_copy)
        self.toolButtonMetaSource.clicked.connect(self.action_open_meta_file)
        self.commandLinkButtonDaisy.clicked.connect(self.action_run_daisy)
        self.toolButtonDaisySource.clicked.connect(
            self.action_open_daisy_source)
        self.commandLinkButtonValidate.clicked.connect(
            self.action_validate_daisy)
        self.commandLinkConfigSave.clicked.connect(self.action_save_config)
        self.commandLinkButtonContinue1.clicked.connect(
            lambda: self.action_tab_select(1))
        self.commandLinkButtonContinue2.clicked.connect(
            lambda: self.action_tab_select(2))
        self.commandLinkButtonContinue3.clicked.connect(
            lambda: self.action_tab_select(3))
        self.commandLinkButtonContinue4.clicked.connect(
            lambda: self.action_tab_select(4))
        self.pushButton_newYear.clicked.connect(
            self.action_save_new_working_year)
        self.pushButtonClose1.clicked.connect(self.action_quit)
        self.pushButtonClose2.clicked.connect(self.action_quit)
        self.pushButtonClose3.clicked.connect(self.action_quit)
        self.pushButtonClose4.clicked.connect(self.action_quit)
        self.pushButtonClose5.clicked.connect(self.action_quit)

    def read_config(self):
        """read Config from file"""
        self.show_debug_message("read Config from file")
        file_exist = os.path.isfile("daisy_creator_mag.config")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEditConsole.append(
                "<font color='red'>"
                + "Config-Datei konnte nicht geladen werden: </font><br>"
                + "daisy_creator_mag.config<br>"
                + "Die Programmnutzung ohne diese Datei ist nicht möglich!")
            log_message = ("Config-Datei konnte nicht geladen werden!<br>"
                           + "Siehe Fehlermeldung")
            self.show_dialog_critical(log_message)
            self.action_tab_select(8)
            return file_exist

        cp = configparser
        config = configparser.RawConfigParser()
        config.read("daisy_creator_mag.config")

        try:
            # load in variables
            self.app_mag_path = config.get('Folders', 'Source')
            self.app_mag_path_dest = config.get('Folders', 'Destination')
            self.app_mag_path_meta = config.get('Folders', 'Meta')
            self.app_mag_path_issue_nr = config.get('Folders',
                                                    'Issue_nr')
            self.app_mag_path_intro = config.get('Folders', 'Intros')
            self.app_mag_path_imprint = config.get('Folders', 'Imprints')
            self.app_mag_path_add_file = config.get('Folders',
                                                    'Files_additionally')
            self.app_mag_prefix_issue_nr = config.get(
                'Files', 'file_prefix_issue_nr')
            self.app_mag_prefix_meta = config.get(
                'Files', 'file_prefix_meta')
            self.app_mag_items = config.get(
                'Magazines', 'Magazines').split(",")
            self.app_validator = config.get('Programs', 'DaisyPipeline')
            self.app_validator_script = config.get(
                'Programs', 'DaisyPipeline_Script')
            self.app_validator_time = config.get(
                'Programs', 'DaisyPipeline_Time')
            self.app_validator_path = config.get(
                'Programs', 'DaisyPipeline_Report')
            # load in config tab entrys
            self.lineEditConfigMagYear.setText(
                config.get('Year', 'business_year'))
            self.lineEditConfigMag.setText(config.get('Folders', 'Source'))
            self.lineEditConfigMagDest.setText(config.get(
                'Folders', 'Destination'))
            self.lineEditConfigMagMeta.setText(config.get(
                'Folders', 'Meta'))
            self.lineEditConfigMagIssueNr.setText(config.get(
                'Folders', 'Issue_nr'))
            self.lineEditConfigMagIntro.setText(config.get(
                'Folders', 'Intros'))
            self.lineEditConfigMagImprints.setText(config.get(
                'Folders', 'Imprints'))
            self.lineEditConfigMagFileAdditionally.setText(config.get(
                'Folders', 'Files_additionally'))
            self.lineEditConfigMagPrefixIssueNr.setText(config.get(
                'Files', 'file_prefix_issue_nr'))
            self.lineEditConfigMagPrefixMeta.setText(config.get(
                'Files', 'file_prefix_meta'))
            self.lineEditConfigMagDesc.setText(config.get(
                'Magazines', 'Magazines'))
            self.lineEditConfigDP.setText(config.get(
                'Programs', 'DaisyPipeline_Package'))
            self.lineEditConfigValidator_1.setText(
                config.get('Programs', 'DaisyPipeline'))
            self.lineEditConfigValidator_2.setText(
                config.get('Programs', 'DaisyPipeline_Script'))
            self.lineEditConfigValidator_3.setText(
                config.get('Programs', 'DaisyPipeline_Time'))
            self.lineEditConfigValidator_4.setText(
                config.get('Programs', 'DaisyPipeline_Report'))
            self.textEditConsole.append("<b>Config-Datei eingelesen:</b>")
            self.textEditConsole.append("daisy_creator_mag.config")

        except (cp.ParsingError, cp.NoSectionError, cp.NoOptionError) as err:
            self.show_debug_message(err.message)
            log_message = ("Fehler beim Laden der Einstellungen:<br>"
                           + err.message)
            self.textEditConsole.append(
                "<font color='red'>" + log_message + "</font>")
            self.action_tab_select(8)
            self.show_dialog_critical(log_message)
            return False

    def read_help(self):
        """read Readme from file"""
        file_exist = os.path.isfile("README.md")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Hilfe-Datei konnte nicht geladen werden: </font>"
                + "README.md")
            return

        fobj = open("README.md")
        for line in fobj:
            self.textEditHelp.append(line)
        # set cursor on top of helpfile
        cursor = self.textEditHelp.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start,
                            QtGui.QTextCursor.MoveAnchor, 0)
        self.textEditHelp.setTextCursor(cursor)
        fobj.close()

    def read_user_help(self):
        """read user help from file"""
        file_exist = os.path.isfile("user_help.md")
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "benutzer-Hilfe-Datei konnte nicht geladen werden: </font>"
                + "user_help.md")
            return

        fobj = open("user_help.md")
        for line in fobj:
            self.textEditUserHelp.append(line)
        # set cursor on top of helpfile
        cursor = self.textEditUserHelp.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start,
                            QtGui.QTextCursor.MoveAnchor, 0)
        self.textEditUserHelp.setTextCursor(cursor)
        fobj.close()

    def action_save_config(self):
        """save config file"""
        display_message = "Config- und Metadateien mit neuen Werten speichern!"
        action = self.show_dialog_question(display_message)
        if action == 1:
            self.backup_config_file()
            self.write_config_file()
            self.make_config_dirs()
            self.copy_config_meta_files()
            self.copy_config_mag_intro_files()
            self.copy_config_mag_imprint_files()
            # load after processing the other stuff
            self.read_config()
            self.progressBarConfig.setValue(80)
            self.change_meta_year()

    def action_save_new_working_year(self):
        """save new_working_year"""
        working_year_old = self.lineEditConfigMagYear.text()
        working_year_new = str(int(self.lineEditConfigMagYear.text()) + 1)
        display_message = ("Neues Arbeitsjahr \n"
                           + working_year_new + "\n für Config vorbereiten?"
                           + "\n \n"
                           + "Zum Abschliessen hier [Save] "
                           + "und dann [Speichern]-Button "
                           + "auf dem Config-Tab klicken!")
        action = self.show_dialog_question(display_message)
        if action == 1:
            self.lineEditConfigMagYear.setText(working_year_new)
            self.lineEditConfigMag.setText(
                self.lineEditConfigMag.text().replace(
                    working_year_old, working_year_new))

            self.lineEditConfigMagDest.setText(
                self.lineEditConfigMagDest.text().replace(
                    working_year_old, working_year_new))

            self.lineEditConfigMagMeta.setText(
                self.lineEditConfigMagMeta.text().replace(
                    working_year_old, working_year_new))

            self.lineEditConfigMagIssueNr.setText(
                self.lineEditConfigMagIssueNr.text().replace(
                    working_year_old, working_year_new))

            self.lineEditConfigMagIntro.setText(
                self.lineEditConfigMagIntro.text().replace(
                    working_year_old, working_year_new))

            self.lineEditConfigMagImprints.setText(
                self.lineEditConfigMagImprints.text().replace(
                    working_year_old, working_year_new))
            self.textEditConfig.append(
                "<font color='red'>" + "Neues Arbeitsjahr vorbereitet:</font> "
                + working_year_new)

    def action_open_copy_source(self):
        """Source of copy"""
        # QtCore.QDir.homePath()
        dir_source = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Quell-Ordner", self.app_mag_path)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_source:
            self.lineEditCopySource.setText(dir_source)
            self.textEdit.append("<b>Quelle:</b>")
            self.textEdit.append(dir_source)
            self.load_filenames()
            if not self.app_orig_source_files:
                error_message = "Der Ordner enthält keine mp3 Dateien!"
                self.show_dialog_critical(error_message)
                return
            self.load_bitrate()
            self.check_toc_from_filenames()

    def action_open_daisy_source(self):
        """Source of daisy"""
        dir_source = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Quell-Ordner", self.app_mag_path_dest)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_source:
            self.lineEditDaisySource.setText(dir_source)
            self.textEdit.append("Quelle:")
            self.textEdit.append(dir_source)

    def action_open_copy_dest(self):
        """Destination for Copy"""
        dir_dest = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Ziel-Ordner", self.app_mag_path_dest)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if dir_dest:
            self.lineEditCopyDest.setText(dir_dest)
            self.textEdit.append("<B>Ziel:</b>")
            self.textEdit.append(dir_dest)

    def action_open_copy_file_1(self):
        """Additional file 1 to copy"""
        file_1 = QtWidgets.QFileDialog.getOpenFileName(
            self, "Datei 1", self.app_mag_path_add_file)
        # Don't attempt to open if open dialog
        # was cancelled away.
        self.show_debug_message(file_1[0])

        if file_1:
            self.lineEditCopyFile1.setText(file_1[0])
            self.textEdit.append("<b>Zusatz-Datei 1</b>:")
            self.textEdit.append(os.path.basename(str(file_1[0])))
            self.check_file_name_1000()

    def action_open_copy_file_2(self):
        """Additional file 2 to copy"""
        file_2 = QtWidgets.QFileDialog.getOpenFileName(
            self, "Datei 2", self.app_mag_path_add_file)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if file_2:
            self.lineEditCopyFile2.setText(file_2[0])
            self.textEdit.append("<b>Zusatz-Datei 2</b>:")
            self.textEdit.append(os.path.basename(str(file_2[0])))

    def action_open_meta_file(self):
        """Metafile to load"""
        meta_file = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Daisy_Meta",
            self.app_mag_path_meta)
        # Don't attempt to open if open dialog
        # was cancelled away.
        if meta_file:
            self.lineEditMetaSource.setText(meta_file[0])
            self.read_meta_file(str(meta_file[0]))

    def action_run_copy(self):
        """Mainfunction to copy"""
        if self.lineEditCopySource.text() == "Quell-Ordner":
            error_message = "Quell-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        if self.lineEditCopyDest.text() == "Ziel-Ordner":
            error_message = "Ziel-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        self.show_debug_message(self.lineEditCopySource.text())
        self.show_debug_message(self.lineEditCopyDest.text())

        # ceck dir of dest
        if os.path.exists(self.lineEditCopyDest.text()) is False:
            error_message = "Ziel-Ordner existiert nicht.."
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            self.lineEditCopyDest.setFocus()
            return

        self.show_debug_message(self.app_orig_source_files)
        n_count_mp3_files = self.count_mp3_files(self.app_orig_source_files)

        if self.checkBoxDaisyLevel.isChecked():
            if self.check_level_from_filenames() is False:
                return

        self.textEdit.append("<b>mp3-Dateien kopieren...</b>")
        if self.checkBoxCopyMagIntro.isChecked():
            self.copy_intro()

        if self.checkBoxCopyMagIssueAnnouncement.isChecked():
            self.copy_issue_nr()

        if self.checkBoxCopyImprint.isChecked():
            self.copy_imprint()

        if self.lineEditCopyFile1.text() != "Datei 1 waehlen":
            self.copy_file_additionally(1)

        if self.lineEditCopyFile2.text() != "Datei 2 waehlen":
            self.copy_file_additionally(2)

        n_counter = 0
        self.show_debug_message(n_count_mp3_files)
        self.app_orig_source_files.sort()
        for item in self.app_orig_source_files:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue

            file_to_copy_source = self.lineEditCopySource.text() + "/" + item
            # check if file exist
            file_exist = os.path.isfile(file_to_copy_source)
            if file_exist is False:
                self.show_debug_message("File not exists")
                # change max number and update progress
                n_count_mp3_files = n_count_mp3_files - 1
                n_count_progress = n_counter * 100 / n_count_mp3_files
                self.progressBarCopy.setValue(int(n_count_progress))
                self.textEdit.append(
                    "<b>Datei konnte nicht kopiert werden: </b><br>"
                    + file_to_copy_source)
                self.show_debug_message(file_to_copy_source)
                self.textEdit.append("<b>Uebersprungen</b>:<br>")
                self.textEdit.append(file_to_copy_source)
                continue

            # cange filenames
            if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                if self.checkBoxDaisyLevel.isChecked():
                    file_to_copy_dest = (
                        item[0:6] + "_"
                        + self.comboBoxCopyMag.currentText()
                        + "_" + self.comboBoxCopyMagIssue.currentText()
                        + "_" + item[7:len(item) - 4] + ".mp3")
                else:
                    file_to_copy_dest = (
                        item[0:4] + "_"
                        + self.comboBoxCopyMag.currentText()
                        + "_" + self.comboBoxCopyMagIssue.currentText()
                        + "_" + item[5:len(item) - 4] + ".mp3")
            else:
                file_to_copy_dest = (
                    item[0:len(item) - 4] + "_"
                    + self.comboBoxCopyMag.currentText()
                    + "_" + self.comboBoxCopyMagIssue.currentText()
                    + "_" + item[0:len(item) - 4] + ".mp3")

            if self.checkBoxCopyChangeNr1001.isChecked():
                # rename 1001.mp3 in 0100
                # to further sort front
                if item[0:4] == "1001":
                    if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                        if self.checkBoxDaisyLevel.isChecked():
                            file_to_copy_dest = (
                                "/0100_" + self.comboBoxCopyMag.currentText()
                                + "_" + self.comboBoxCopyMagIssue.currentText()
                                + "_" + item[6:len(item) - 4] + ".mp3")
                        else:
                            file_to_copy_dest = (
                                "/0100_" + self.comboBoxCopyMag.currentText()
                                + "_" + self.comboBoxCopyMagIssue.currentText()
                                + "_" + item[5:len(item) - 4] + ".mp3")
                    else:
                        file_to_copy_dest = (
                            "/0100_" + self.comboBoxCopyMag.currentText()
                            + "_" + self.comboBoxCopyMagIssue.currentText()
                            + "_" + item[0:len(item) - 4]
                            + ".mp3")

                    self.textEdit.append(
                        "<b>1000.mp3 in 0100 umbenannt "
                        "damit die Ansage weiter vorn einsortiert wird</b>")

            if self.checkBoxCopyChangeNr1000.isChecked():
                # rename 1000.mp3 in 0100
                # to further sort front
                if item[0:4] == "1000":
                    if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                        if self.checkBoxDaisyLevel.isChecked():
                            file_to_copy_dest = (
                                "/0100_" + self.comboBoxCopyMag.currentText()
                                + "_" + self.comboBoxCopyMagIssue.currentText()
                                + "_" + item[6:len(item) - 4] + ".mp3")
                        else:
                            file_to_copy_dest = (
                                "/0100_" + self.comboBoxCopyMag.currentText()
                                + "_" + self.comboBoxCopyMagIssue.currentText()
                                + "_" + item[5:len(item) - 4] + ".mp3")
                    else:
                        file_to_copy_dest = (
                            "/0100_" + self.comboBoxCopyMag.currentText()
                            + "_" + self.comboBoxCopyMagIssue.currentText()
                            + "_" + item[0:len(item) - 4] + ".mp3")

                    self.textEdit.append(
                        "<b>1000.mp3 in 0100 umbenannt "
                        "damit die Ansage weiter vorn einsortiert wird</b>")

            if self.checkBoxCopyFileNameShort.isChecked():
                # shorten dest file name to 60 characters
                file_to_copy_dest = self.check_filename_lenght(
                    file_to_copy_dest)

            # complete with path
            file_to_copy_dest = (self.lineEditCopyDest.text()
                                 + "/" + str(file_to_copy_dest))

            # self.textEdit.append(file_to_copy_dest)
            self.show_debug_message(file_to_copy_source)
            self.show_debug_message(file_to_copy_dest)

            # check bitrate
            bitrate_ok = self.check_bitrate(
                file_to_copy_source, file_to_copy_dest)
            # nothing to do, only copy
            if bitrate_ok:
                file_to_copy_dest = (
                    self.fit_chars_in_filename(
                        file_to_copy_dest))
                self.copy_file(file_to_copy_source, file_to_copy_dest)
            else:
                self.textEdit.append("<b>Kopieren...</b>")
            n_counter += 1
            self.show_debug_message(n_counter)
            self.show_debug_message(n_count_mp3_files)
            n_count_progress = n_counter * 100 / n_count_mp3_files
            self.show_debug_message(n_count_progress)
            self.progressBarCopy.setValue(int(n_count_progress))

        self.show_debug_message(n_counter)

        self.textEdit.append("<b>Kopieren abgeschlossen</b><br>")

        # recode
        self.show_debug_message(self.dict_files_to_recode)
        self.change_bitrate_and_copy()

        # load metadata
        self.lineEditMetaSource.setText(
            self.app_mag_path_meta + "/" + self.app_mag_prefix_meta + "_"
            + str(self.comboBoxCopyMag.currentText()))
        self.read_meta_file(str(self.lineEditMetaSource.text()))
        # enter path of source and destination
        self.lineEditDaisySource.setText(self.lineEditCopyDest.text())

    def backup_config_file(self):
        """backup config file """
        try:
            shutil.copy(
                "daisy_creator_mag.config", "daisy_creator_mag.config.bak")
            self.show_debug_message("backup config file")
            self.textEditConfig.append("<b>Config-Datei gesichert:</b>")
            self.textEditConfig.append("daisy_creator_mag.config.bak")
            self.progressBarConfig.setValue(10)
        except OSError as e:
            log_message = "copy_file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message
                + "daisy_creator_mag.config</font>")

    def change_meta_year(self):
        """change meta year"""
        self.textEditConfig.append("<b>Meta-Datei angepasst:</b>")
        try:
            files_source = os.listdir(str(self.lineEditConfigMagMeta.text()))
            if not files_source:
                return

            for item in files_source:
                # filter out backup files
                if item[len(item) - 1:len(item)] == "~":
                    continue
                path_file_source = (str(self.lineEditConfigMagMeta.text())
                                    + "/" + item)
                print(path_file_source)
                self.read_meta_file(path_file_source)
                self.write_meta_file(path_file_source)
                self.textEditConfig.append(item)
            self.progressBarConfig.setValue(100)
        except Exception as e:
            log_message = "change meta file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message + "</font>")

    def copy_file(self, file_to_copy_source, file_to_copy_dest):
        """copy file"""
        try:
            shutil.copy(file_to_copy_source, file_to_copy_dest)
            self.textEdit.append(
                os.path.basename(str(file_to_copy_dest)))
        except OSError as e:
            log_message = "copy_file Error: %s" % str(e).decode('utf-8')
            self.show_debug_message(log_message)
            self.textEdit.append(
                "<font color='red'>" + log_message
                + file_to_copy_dest + "</font>")

    def copy_config_meta_files(self):
        """copy config meta files"""
        self.textEditConfig.append("<b>Meta-Dateien kopieren:</b>")

        files_source = os.listdir(self.app_mag_path_meta)
        if not files_source:
            return

        for item in files_source:
            # filter out backup files
            if item[len(item) - 1:len(item)] == "~":
                continue
            path_file_source = self.app_mag_path_meta + "/" + item
            path_file_dest = (str(self.lineEditConfigMagMeta.text())
                              + "/" + item)
            try:
                shutil.copy(path_file_source, path_file_dest)
            except OSError as e:
                log_message = "copy_file Error: %s" % str(e).decode('utf-8')
                self.show_debug_message(log_message)
                self.textEditConfig.append(
                    "<font color='red'>" + log_message + "</font>")
            self.textEditConfig.append(item)

        self.progressBarConfig.setValue(40)

    def copy_config_mag_intro_files(self):
        """copy config mag issue nr files"""
        self.textEditConfig.append("Intro-Dateien kopieren:")

        files_source = os.listdir(self.app_mag_path_intro)
        if not files_source:
            return

        for item in files_source:
            # filter out backup files
            if item[len(item) - 1:len(item)] == "~":
                continue
            path_file_source = self.app_mag_path_intro + "/" + item
            # print path_file_source
            path_file_dest = (str(self.lineEditConfigMagIntro.text())
                              + "/" + item)
            try:
                shutil.copy(path_file_source, path_file_dest)
            except OSError as e:
                log_message = "copy_file Error: %s" % str(e).decode('utf-8')
                self.show_debug_message(log_message)
                self.textEditConfig.append(
                    "<font color='red'>" + log_message + "</font>")
            self.textEditConfig.append(item)
        self.progressBarConfig.setValue(50)

    def copy_config_mag_imprint_files(self):
        """copy config magazin imprint files"""
        self.textEditConfig.append("<b>Impressums-Dateien kopieren:</b>")

        files_source = os.listdir(self.app_mag_path_imprint)
        # print files_source
        if not files_source:
            return

        for item in files_source:
            # filter out backup files
            if item[len(item) - 1:len(item)] == "~":
                continue
            # filter out non mp3 files
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue

            path_file_source = self.app_mag_path_imprint + "/" + item

            # print path_file_source
            path_file_dest = (str(self.lineEditConfigMagImprints.text())
                              + "/" + item)
            try:
                shutil.copy(path_file_source, path_file_dest)
            except OSError as e:
                log_message = "copy_file Error: %s" % str(e)
                self.show_debug_message(log_message)
                self.textEditConfig.append(
                    "<font color='red'>" + log_message + "</font>")
            self.textEditConfig.append(item)
        self.progressBarConfig.setValue(60)

    def copy_intro(self):
        """copy Intro"""
        file_to_copy_source = (self.app_mag_path_intro + "/Intro_"
                               + self.comboBoxCopyMag.currentText() + ".mp3")

        # set numeric prefix for sort the intro in the right order
        # number 13 so that the intro file follows issue_nr file
        # that is corresponent to months
        if self.checkBoxDaisyLevel.isChecked():
            # include level in filename
            file_to_copy_dest = (self.lineEditCopyDest.text() + "/0013_1_"
                                 + self.comboBoxCopyMag.currentText() + "_"
                                 + self.comboBoxCopyMagIssue.currentText()
                                 + "_Intro.mp3")
        else:
            file_to_copy_dest = (self.lineEditCopyDest.text() + "/0013_"
                                 + self.comboBoxCopyMag.currentText() + "_"
                                 + self.comboBoxCopyMagIssue.currentText()
                                 + "_Intro.mp3")

        self.show_debug_message(file_to_copy_source)
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Intro nicht vorhanden</font>: "
                + os.path.basename(str(file_to_copy_source)))
            return

        # check bitrate, when necessary recode later in new destination
        self.show_debug_message("Bitrate check: " + file_to_copy_source)

        bitrate_ok = self.check_bitrate(
            file_to_copy_source, str(file_to_copy_dest))
        # nothing to do, only copy
        if bitrate_ok:
            file_to_copy_dest = (
                self.fit_chars_in_filename(file_to_copy_dest))
            self.copy_file(file_to_copy_source, file_to_copy_dest)
        else:
            self.textEdit.append("<b>Kopieren...</b>")

    def copy_issue_nr(self):
        """copy issue number"""
        path_issue = (self.app_mag_path_issue_nr
                      + "/" + self.app_mag_prefix_issue_nr
                      + "_" + self.comboBoxCopyMag.currentText())
        self.show_debug_message(path_issue)
        file_to_copy_source = (
            path_issue + "/0001_"
            + self.comboBoxCopyMag.currentText() + "_"
            + self.comboBoxCopyMagIssue.currentText() + "_Ausgabe.mp3")
        file_to_copy_dest = (
            self.lineEditCopyDest.text() + "/0001_"
            + self.comboBoxCopyMag.currentText() + "_"
            + self.comboBoxCopyMagIssue.currentText() + "_Ausgabe.mp3")
        self.show_debug_message(file_to_copy_source)
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Ausgabeansage nicht vorhanden</font>:<br>"
                + os.path.basename(str(file_to_copy_source)))
            return

        bitrate_ok = self.check_bitrate(
            file_to_copy_source, str(file_to_copy_dest))
        # nothing to do, only copy
        if bitrate_ok:
            file_to_copy_dest = (
                self.fit_chars_in_filename(file_to_copy_dest))
            self.copy_file(file_to_copy_source, file_to_copy_dest)
        else:
            self.textEdit.append("<b>Kopieren...</b>")

    def copy_imprint(self):
        """copy imprint"""
        file_to_copy_source = (self.app_mag_path_imprint + "/Impressum_"
                               + self.comboBoxCopyMag.currentText() + ".mp3")

        if self.checkBoxDaisyLevel.isChecked():
            # include level in filename
            file_to_copy_dest = (self.lineEditCopyDest.text() + "/2000_1_"
                                 + self.comboBoxCopyMag.currentText() + "_"
                                 + self.comboBoxCopyMagIssue.currentText()
                                 + "_Impressum.mp3")
        else:
            file_to_copy_dest = (self.lineEditCopyDest.text() + "/2000_"
                                 + self.comboBoxCopyMag.currentText() + "_"
                                 + self.comboBoxCopyMagIssue.currentText()
                                 + "_Impressum.mp3")

        self.show_debug_message(file_to_copy_source)
        self.show_debug_message(file_to_copy_dest)

        file_exist = os.path.isfile(file_to_copy_source)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Impressum nicht vorhanden</font>: "
                + os.path.basename(str(file_to_copy_source)))
            return

        # check bitrate, when necessary recode later in new destination
        self.show_debug_message("Bitrate check: " + file_to_copy_source)

        bitrate_ok = self.check_bitrate(
            file_to_copy_source, str(file_to_copy_dest))
        # nothing to do, only copy
        if bitrate_ok:
            file_to_copy_dest = (
                self.fit_chars_in_filename(file_to_copy_dest))
            self.copy_file(file_to_copy_source, file_to_copy_dest)
        else:
            self.textEdit.append("<b>Kopieren...</b>")

    def copy_file_additionally(self, n):
        """copy additional file"""
        if n == 1:
            filename = os.path.basename(str(self.lineEditCopyFile1.text()))
            path_filename_source = str(self.lineEditCopyFile1.text())

        if n == 2:
            filename = os.path.basename(str(self.lineEditCopyFile2.text()))
            path_filename_source = str(self.lineEditCopyFile2.text())

        if self.checkBoxCopyFileNameShort.isChecked():
            # shorten dest file name to 60 characters
            filename_checked = self.check_filename_lenght(filename)

        file_to_copy_dest = (str(self.lineEditCopyDest.text())
                             + "/" + filename_checked)

        # check bitrate, when necessary recode in new destination
        self.show_debug_message("Bitrate check: " + filename)

        bitrate_ok = self.check_bitrate(
            path_filename_source, str(file_to_copy_dest))
        # nothing to do, only copy
        if bitrate_ok:
            file_to_copy_dest = (
                self.fit_chars_in_filename(file_to_copy_dest))
            self.copy_file(path_filename_source, file_to_copy_dest)

    def check_modules(self, python_module):
        """
        check if python-module is installed
        needs importlib
        """
        spec = importlib.util.find_spec(python_module)
        if spec is None:
            error_message = ("Es fehlt das Python-Modul:\n " + python_module
                             + "\nZur Nutzung des vollen Funktionsumfanges "
                             + "muss es installiert werden!")
            self.show_dialog_critical(error_message)
            self.textEdit.append(
                    "<font color='red'>Es fehlt das Paket: </font> "
                    + python_module)

    def fit_chars_in_filename(self, file_name):
        """replace spaces and non ascii characters"""
        if file_name.find(" ") != -1:
            file_name = file_name.replace(" ", "_")
            self.textEdit.append(
                "<font color='blue'>Leerzeichen ersetzt bei:</font><br>"
                + os.path.basename(str(file_name)))

        f_name_none_umlauts = file_name
        umlauts_dict = {
            'ä': 'ae',
            'ö': 'oe',
            'ü': 'ue',
            'Ä': 'Ae',
            'Ö': 'Oe',
            'Ü': 'Ue',
            'ß': 'ss',
            }
        for c in umlauts_dict.keys():
            f_name_none_umlauts = f_name_none_umlauts.replace(
                c, umlauts_dict[c])

        if file_name != f_name_none_umlauts:
            self.textEdit.append(
                "<font color='blue'>"
                + "Umlaute in ASCII umgewandelt bei:"
                + "</font><br>"
                + os.path.basename(str(file_name)))
            file_name = f_name_none_umlauts

        for c in file_name:
            if ord(c) > 128:
                self.textEdit.append(
                    "<font color='blue'>"
                    + "Unerlaubtes Zeichen ersetzen bei:</font><br>"
                    + os.path.basename(str(file_name)))
        return "".join(c for c in file_name if ord(c) < 128)

    def load_filenames(self):
        """laod filenames"""

        try:
            self.app_orig_source_files = (
                os.listdir(self.lineEditCopySource.text()))
        except OSError as e:
            error_message = "Quelle: %s" % str(e)
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            return
        self.textEdit.append("<br><b>mp3 Dateien in Quelle</b>:")

        self.app_orig_source_files.sort()
        for item in self.app_orig_source_files:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue
            self.textEdit.append(item)

    def check_toc_from_filenames(self):
        """check if filenames contain not only for numbers
        if so, set TOC from filename True"""
        self.show_debug_message("check_toc_from_filenames")
        n = 0
        for item in self.app_orig_source_files:
            # check a few charakters of is alpha
            # after 4 digits and bevor file extention
            if len(item) > 4 and len(item) > 15:
                if item[7:10].isalpha():
                    n += 1

        if n > 1:
            self.textEdit.append(
                "<b>Dateinamen könnten für TOC benutzt werden</b>")
            self.checkBoxDaisyIgnoreTitleDigits.setChecked(True)
            self.checkBoxDaisyIgnoreTitleDigits.setStyleSheet(
                "QCheckBox" "{" "background-color: 'dark magenta';" "}")
            self.textEdit.append("Titelliste:")
            # all titles from filename in a list
            for item in self.app_orig_source_files:
                if item.find("_") != -1:
                    title = item.replace("_", " ")
                    self.list_titles_from_files.append(title[0:len(title) - 4])
                    self.textEdit.append(title[5:len(item) - 4])
                else:
                    self.list_titles_from_files.append(item[0:len(item) - 4])
                    self.textEdit.append(item[5:len(item) - 4])

            self.show_debug_message(self.list_titles_from_files)
        else:
            self.textEdit.append(
                "Dateinamen scheinbar nicht für TOC geeignet<br>")

    def check_file_name_1000(self):
        """check file name 1000 """
        self.show_debug_message("check file name 1000")
        for item in self.app_orig_source_files:
            if item[:4] == "1000":
                self.textEdit.append(
                    "<b>Datei, beginnend mit 1000 ist vorhanden</b>")
                self.textEdit.append(
                    "Option zur Umbenennung der Datei 1000 wurde eingestellt.")
                self.checkBoxCopyChangeNr1000.setChecked(True)
                self.checkBoxCopyChangeNr1000.setStyleSheet(
                    "QCheckBox" "{" "background-color: 'dark magenta';" "}")
                break
            if item[:4] == "1001":
                self.textEdit.append(
                    "<b>Datei, beginnend mit 1001 ist vorhanden</b>")
                self.textEdit.append(
                    "Option zur Umbenennung der Datei 1001 wurde eingestellt.")
                self.checkBoxCopyChangeNr1001.setChecked(True)
                self.checkBoxCopyChangeNr1001.setStyleSheet(
                    "QCheckBox" "{" "background-color: 'dark magenta';" "}")

    def check_filename_lenght(self, filename_to_check):
        """check lengt of filename, and shortening if necessary"""
        if len(filename_to_check) > 60:
            # base name without ext.
            file_name_temp = filename_to_check[0:len(filename_to_check) - 4]
            file_name_changed = file_name_temp[0:55] + ".mp3"
            self.textEdit.append(
                "<font color='blue'>"
                + "Dateiname gekuerzt:</font><br>"
                + file_name_changed)
            return file_name_changed
        else:
            return filename_to_check

    def check_level_from_filenames(self):
        """check if filenames contains the right syntax
        for geenerating levels, for example: 1002_1_title.mp3"""
        self.show_debug_message("check_level_from_filenames")
        self.textEdit.append(
            "<br><b>"
            + "Prüfen ob Ebenen aus Dateinamen definiert werden können...</b>")
        # if re.match("\d{1,}", item[5:6]) is not None:
        n = 0
        for item in self.app_orig_source_files:
            #
            if re.match("\d{1,}", item[5:6]) is None:
                if n == 0:
                    self.textEdit.append(
                        "Ebenen-Syntax scheint zu fehlen bei:<b>")
                self.textEdit.append(item)
                n += 1

        if n > 0:
            self.textEdit.append(
                "<b>Mindestens bei einer Datei "
                + "scheint die Ebenen-Nr zu fehlen."
                + "<br>Kopieren abgebrochen!</b>")

            self.checkBoxDaisyLevel.setStyleSheet(
                "QCheckBox" "{" "background-color: 'dark magenta';" "}")
            self.textEdit.append("Siehe oben!")
            return False
        else:
            return True

    def count_mp3_files(self, files_source):
        """count mp3 files"""
        self.show_debug_message("count mp3 files")
        z = 0
        for item in files_source:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue
            else:
                z += 1
        return z

    def check_change_id3_preparation(self):
        """check id3 Tags, maybe kill it, preparation"""
        # check for files in dest
        self.textEdit.append(
            "<b>ID3-Tags pruefen...</b>")

        try:
            dir_dest = os.listdir(self.lineEditCopyDest.text())
        except OSError as e:
            error_message = "Quelle: %s" % str(e)
            self.show_debug_message(error_message)
            self.show_dialog_critical(error_message)
            return

        is_changed = False
        dir_dest.sort()
        self.show_debug_message(dir_dest)
        for item in dir_dest:
            file_in_dest = self.lineEditCopyDest.text() + "/" + item
            self.textEdit.append(item)
            self.show_debug_message(file_in_dest)
            # check if file exist
            file_exist = os.path.isfile(file_in_dest)
            if file_exist is False:
                self.show_debug_message("File not exists")
                self.textEdit.append(
                    "<b>Datei konnte nicht auf ID3 geprueft werden: </b><br>"
                    + file_in_dest)
                self.show_debug_message(file_in_dest)
                self.textEdit.append("<b>Uebersprungen</b>:<br>")
                self.textEdit.append(file_in_dest)
                continue
            is_changed = self.check_change_id3(file_in_dest)

            if is_changed:
                self.textEdit.append("<b>ID3-Tags pruefen: </b>")

        self.textEdit.append("<b>ID3-Tag-Pruefung abgeschlossen </b>")

    def check_change_id3(self, file_in_dest):
        """check id3 Tags, maybe kill it"""
        tag = None
        is_changed = False
        try:
            audio = ID3(str(file_in_dest))
            tag = "yes"
        except ID3NoHeaderError:
            self.show_debug_message("No ID3 header found; skipping.")

        if tag is not None:
            if self.checkBoxCopyID3Change.isChecked():
                audio.delete()
                is_changed = True
                self.textEdit.append("<b>ID3 entfernt bei</b>:<br>"
                                     + os.path.basename(str(file_in_dest)))
                self.show_debug_message(
                    "ID3 entfernt bei " + file_in_dest)
            else:
                self.textEdit.append(
                    "<b>ID3 vorhanden, aber NICHT entfernt bei</b>:<br>"
                    + file_in_dest)

        return is_changed

    def load_bitrate(self):
        """load bitrate,
        check bitrate and switch to most used one"""
        # self.show_debug_message("load bitrate...")
        self.textEdit.append(
            "<br><b>Bitrate der mp3 Dateien ermitteln...</b>")
        dict_birate = {}
        n_bitrate = 0

        for item in self.app_orig_source_files:
            if (item[len(item) - 4:len(item)] != ".MP3"
                    and item[len(item) - 4:len(item)] != ".mp3"):
                continue
            # self.textEdit.append(item)
            file_to_check = self.lineEditCopySource.text() + "/" + item
            audio_source = MP3(str(file_to_check))
            n_bitrate = audio_source.info.bitrate / 1000

            if not dict_birate:
                dict_birate[n_bitrate] = 1
            else:
                if n_bitrate in dict_birate:
                    dict_birate[n_bitrate] = dict_birate[n_bitrate] + 1
                else:
                    dict_birate[n_bitrate] = 1

        dict_birate_s = sorted(
            dict_birate.items(), key=operator.itemgetter(1), reverse=True)

        for k, v in dict_birate_s:
            self.textEdit.append(
                str(v) + " Dateie(n) mit " + str(k) + " kBit/s")

        item_combo = 0
        while item_combo < self.comboBoxPrefBitrate.count():
            self.comboBoxPrefBitrate.setCurrentIndex(item_combo)
            if (int(self.comboBoxPrefBitrate.currentText())
                    == int(dict_birate_s[0][0])):
                self.comboBoxPrefBitrate.setStyleSheet(
                    "QComboBox" "{" "background-color: 'dark magenta';" "}")
                self.textEdit.append(
                    "<b>Datenrate eingestellt auf "
                    + str(dict_birate_s[0][0]) + "<b>")
                break
            item_combo += 1

    def check_bitrate(self, file_to_copy_source, file_to_copy_dest):
        """check bitrate,
        when necessary add to dict for later recode in new destination"""

        audio_source = MP3(str(file_to_copy_source))
        if (audio_source.info.bitrate / 1000
                == int(self.comboBoxPrefBitrate.currentText())):
            if self.checkBoxCopyBitrateChange.isChecked():
                return True
            else:
                self.textEdit.append(
                    "<b>Bitrate wurde NICHT geaendern bei</b><br>: "
                    + file_to_copy_dest)
                return True

        # we need recoding, add to dict
        file_to_copy_dest = (
            self.fit_chars_in_filename(file_to_copy_dest))
        self.dict_files_to_recode[file_to_copy_source] = file_to_copy_dest
        self.textEdit.append(
            "<b>Vom Kopieren zurückgestellt für Aenderung der Bitrate</b>:<br>"
            + os.path.basename(str(file_to_copy_source)))
        return None

    def change_bitrate_and_copy(self):
        """recode in new destination"""

        if not self.dict_files_to_recode:
            self.textEdit.append("<b>Bitrate aendern nicht noetig</b>")
            return

        self.textEdit.append("<b>Bitrate aendern...</b>")
        tasks = []
        for s, d in self.dict_files_to_recode.items():
            self.textEdit.append(os.path.basename(str(s)))
            tasks.append(
                ("lame",
                 ["-b", self.comboBoxPrefBitrate.currentText(),
                  "-m", "m", s, d]))

        self.show_debug_message(tasks)
        self.progressBarCopy.setMaximum(len(tasks))
        self.textEditConsole.clear()
        self.commandLinkButton.setStyleSheet("background-color: red")
        self.commandLinkButton.setEnabled(False)
        self.commandLinkButtonDaisy.setEnabled(False)
        self.commandLinkButtonValidate.setEnabled(False)
        self.movie.start()
        self.manager_bitrate.finished.connect(self.process_bitrate_finished)
        self.manager_bitrate.start_tasks(tasks)

    def action_validate_daisy(self):
        """validate daisy"""
        self.textEditValidate.append("<b>Daisy-Validierung...</b><br>")
        self.show_debug_message("validate daisy")
        # characterset of commands is importand
        # encoding in the right manner

        ncc_file = os.path.join(
            str(self.lineEditDaisySource.text()), "ncc.html")
        daisy_validator_out_path = os.path.join(
            self.app_mag_path, self.app_validator_path)

        try:
            command = (
                self.app_validator + " "
                + self.app_validator_script
                + " --ncc "
                + ncc_file
                + " --output "
                + daisy_validator_out_path
                + " --timeToleranceMs "
                + self.app_validator_time)
            self.textEditConsole.clear()
            self.commandLinkButtonValidate.setStyleSheet(
                "background-color: red")
            self.commandLinkButton.setEnabled(False)
            self.commandLinkButtonDaisy.setEnabled(False)
            self.commandLinkButtonValidate.setEnabled(False)
            self.process_validator.finished.connect(
                self.process_validator_finished)
            self.process_validator.start(command)
            self.movie.start()
            self.labelWaitValidate.show()

        except Exception as e:
            errMessage = "Fehler beim Validieren: %s" % str(e)
            self.show_debug_message(errMessage)
            self.show_dialog_critical(errMessage)
            self.textEditValidate.append(
                "<font color='red'>Fehler beim Validieren</font>")

    def validate_daisy_load_report(self):
        """validate daisy load report"""

        self.textEditValidate.append("<b>Report</b>:")

        daisy_validator_out_path = os.path.join(
            self.app_mag_path, self.app_validator_path)
        daisy_validator_out_file = (
            os.path.join(
                daisy_validator_out_path,
                "html-report/html-report.html"))

        try:
            html = open(daisy_validator_out_file).read()
            self.textEditValidate.append(html2text.html2text(html))

        except Exception as e:
            errMessage = "Fehler beim Laden des Reports: %s" % str(e)
            self.show_debug_message(errMessage)
            self.show_dialog_critical(errMessage)
            self.textEditValidate.append(
                "<font color='red'>Fehler beim Laden des Reports</font>")

    def read_meta_file(self, meta_file):
        """load file with meta-data"""
        # print "read meta"
        # print meta_file
        if meta_file is None:
            meta_file = (str(self.lineEditMetaSource.text())
                         + "/" + self.app_mag_prefix_meta)
            # print "is none"

        file_exist = os.path.isfile(meta_file)
        if file_exist is False:
            self.show_debug_message("File not exists")
            self.textEdit.append(
                "<font color='red'>"
                + "Meta-Datei konnte nicht geladen werden</font>: "
                + os.path.basename(meta_file))
            return

        config = configparser.RawConfigParser()
        config.read(meta_file)
        try:
            self.lineEditMetaProducer.setText(
                config.get('Daisy_Meta', 'Produzent'))
            self.lineEditMetaAutor.setText(config.get('Daisy_Meta', 'Autor'))
            self.lineEditMetaTitle.setText(config.get('Daisy_Meta', 'Titel'))
            self.lineEditMetaEdition.setText(
                config.get('Daisy_Meta', 'Edition'))
            self.lineEditMetaNarrator.setText(
                config.get('Daisy_Meta', 'Sprecher'))
            self.lineEditMetaKeywords.setText(
                config.get('Daisy_Meta', 'Stichworte'))
            self.lineEditMetaRefOrig.setText(
                config.get('Daisy_Meta', 'ISBN/Ref-Nr.Original'))
            self.lineEditMetaPublisher.setText(
                config.get('Daisy_Meta', 'Verlag'))
            self.lineEditMetaYear.setText(config.get('Daisy_Meta', 'Jahr'))
        except (configparser.NoSectionError, configparser.NoOptionError):
            log_message = ("Fehler beim laden der Meta-Daten:"
                           + "Sektion oder Option fehlt")
            self.show_debug_message(log_message)
            self.textEditConfig.append(
                "<font color='red'>" + log_message + "</font>")
            self.show_dialog_critical(log_message)

    def action_run_daisy(self):
        """create daisy-fileset"""
        if self.lineEditDaisySource.text() == "Quell-Ordner":
            error_message = "Quell-Ordner wurde nicht ausgewaehlt.."
            self.show_dialog_critical(error_message)
            return

        # read audiofiles
        try:
            dir_items = os.listdir(self.lineEditDaisySource.text())
        except OSError as e:
            log_message = "read_files_from_dir Error: %s" % str(e)
            self.show_debug_message(log_message)

        self.progressBarDaisy.setValue(10)
        self.show_debug_message(dir_items)
        self.textEditDaisy.append("<b>Folgende Audios werden bearbeitet:</b>")
        n_mp3 = 0
        n_list = len(dir_items)
        self.show_debug_message(n_list)
        dir_audios = []
        dir_items.sort()

        for item in dir_items:
            if (item[len(item) - 4:len(item)] == ".MP3"
                    or item[len(item) - 4:len(item)] == ".mp3"):
                dir_audios.append(item)
                self.textEditDaisy.append(item)
                n_mp3 += 1

        # n_total_audio_length = self.calc_audio_lengt(dir_audios)
        l_times = self.calc_audio_lengt(dir_audios)
        n_total_audio_length = l_times[0]
        l_total_elapsed_time = l_times[1]
        l_file_time = l_times[2]
        # print n_total_audio_length
        totalTime = timedelta(seconds=n_total_audio_length)
        # change from timedelta in to string
        # hours, minits and seconds must have 2 digits (zfill(8))

        lTotalTime = str(totalTime).split(".")
        c_total_time = lTotalTime[0].zfill(8)
        # str(c_total_time[0]).zfill(8)
        self.textEditDaisy.append("Gesamtlaenge: " + c_total_time)
        self.write_NCC(c_total_time, n_mp3, dir_audios)
        self.progressBarDaisy.setValue(20)
        self.write_master_smil(c_total_time, dir_audios)
        self.progressBarDaisy.setValue(50)
        self.write_smil(l_total_elapsed_time, l_file_time, dir_audios)
        self.progressBarDaisy.setValue(100)

    def calc_audio_lengt(self, dir_audios):
        """calc total length"""
        n_total_audio_length = 0
        l_total_elapsed_time = []
        l_total_elapsed_time.append(0)
        l_file_time = []
        for item in dir_audios:
            fileToCheck = os.path.join(
                str(self.lineEditDaisySource.text()), item)
            audio_source = MP3(fileToCheck)
            self.show_debug_message(item + " " + str(audio_source.info.length))
            n_total_audio_length += audio_source.info.length
            l_total_elapsed_time.append(n_total_audio_length)
            l_file_time.append(audio_source.info.length)
            l_times = []
            l_times.append(n_total_audio_length)
            l_times.append(l_total_elapsed_time)
            l_times.append(l_file_time)
        return l_times

    def write_NCC(self, c_total_time, n_mp3, dir_audios):
        """write NCC-Page"""
        # Levels
        max_level = "1"
        # find max-level
        if self.checkBoxDaisyLevel.isChecked():
            for item in dir_audios:
                self.show_debug_message("Level: " + item[5:6])
                if re.match("\d{1,}", item[5:6]) is not None:
                    if item[5:6] > max_level:
                        max_level = item[5:6]

        try:
            f_out_file = open(
                os.path.join(
                    str(self.lineEditDaisySource.text()), "ncc.html"), 'w')
        except IOError as err:
            (errno, strerror) = err.args
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return
        # else:
        self.textEditDaisy.append("<b>NCC-Datei schreiben...</b>")
        f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
        f_out_file.write(
            '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0'
            + ' Transitional//EN"'
            + ' "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
            + '\r\n')
        f_out_file.write('<html xmlns="http://www.w3.org/1999/xhtml">'
                         + '\r\n')
        f_out_file.write('<head>' + '\r\n')
        f_out_file.write('<meta http-equiv="Content-type" '
                         + 'content="text/html; charset=utf-8"/>' + '\r\n')
        f_out_file.write('<title>' + self.comboBoxCopyMag.currentText()
                         + '</title>' + '\r\n')

        f_out_file.write('<meta name="ncc:generator" '
                         + 'content="KOM-IN-DaisyCreator"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:revision" content="1"/>' + '\r\n')
        today = datetime.date.today()
        f_out_file.write('<meta name="ncc:producedDate" content="'
                         + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:revisionDate" content="'
                         + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:tocItems" content="'
                         + str(n_mp3) + '"/>' + '\r\n')

        f_out_file.write('<meta name="ncc:totalTime" content="'
                         + c_total_time + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:narrator" content="'
                         + self.lineEditMetaNarrator.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageNormal" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageFront" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:pageSpecial" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sidebars" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:prodNotes" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:footnotes" content="0"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:depth" content="' + max_level + '"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:maxPageNormal" content="0"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:charset" content="utf-8"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:multimediaType" content="audioNcc"/>'
                         + '\r\n')
        # f_out_file.write( '<meta name="ncc:kByteSize" content=" "/>'+ '\r\n')
        f_out_file.write('<meta name="ncc:setInfo" content="1 of 1"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:sourceDate" content="'
                         + self.lineEditMetaYear.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sourceEdition" content="'
                         + self.lineEditMetaEdition.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:sourcePublisher" content="'
                         + self.lineEditMetaPublisher.text() + '"/>' + '\r\n')

        # Anzahl files = Records 2x + ncc.html + master.smil
        f_out_file.write('<meta name="ncc:files" content="'
                         + str(n_mp3 + n_mp3 + 2) + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:producer" content="'
                         + self.lineEditMetaProducer.text() + '"/>' + '\r\n')

        f_out_file.write('<meta name="dc:creator" content="'
                         + self.lineEditMetaAutor.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:date" content="'
                         + today.strftime("%Y-%m-%d") + '"/>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
        f_out_file.write('<meta name="dc:identifier" content="'
                         + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:language" content="de"'
                         + ' scheme="ISO 639"/>' + '\r\n')
        f_out_file.write('<meta name="dc:publisher" content="'
                         + self.lineEditMetaPublisher.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:source" content="'
                         + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:subject" content="'
                         + self.lineEditMetaKeywords.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:title" content="'
                         + self.lineEditMetaTitle.text() + '"/>' + '\r\n')
        # Medibus-OK items
        f_out_file.write(
            '<meta name="prod:audioformat" content="wave 44 kHz"/>' + '\r\n')
        f_out_file.write(
            '<meta name="prod:compression" content="mp3 '
            + self.comboBoxPrefBitrate.currentText() + '/ kb/s"/>' + '\r\n')
        f_out_file.write('<meta name="prod:localID" content=" "/>' + '\r\n')
        f_out_file.write('</head>' + '\r\n')
        f_out_file.write('<body>' + '\r\n')
        n_counter = 0
        for item in dir_audios:
            n_counter += 1
            if n_counter == 1:
                f_out_file.write(
                    '<h1 class="title" id="cnt_0001">'
                    + '<a href="0001.smil#txt_0001">'
                    # + self.lineEditMetaAutor.text() + ": "
                    + self.lineEditMetaTitle.text()
                    + '</a></h1>' + '\r\n')
                continue

            # extract title
            self.show_debug_message("extract title...")

            if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                for title in self.list_titles_from_files:
                    self.show_debug_message("title:")
                    self.show_debug_message(title)
                    self.show_debug_message("item:")
                    self.show_debug_message(item)
                    if title[:4] == item[:4]:
                        if self.checkBoxDaisyLevel.isChecked():
                            c_title = title[7:]
                        else:
                            c_title = title[5:]
                        break

            item_splitted = self.split_filename(item)
            c_title = self.extract_title(item_splitted)

            self.show_debug_message("item_splitted:")
            self.show_debug_message(item_splitted)
            self.show_debug_message("extracted title:")
            self.show_debug_message(c_title)
            # self.show_debug_message(c_title[2:4])
            # self.show_debug_message(c_title[2:2])

            # mag specials
            # Date as title for calendars
            if self.checkBoxDaisyDateCalendar.isChecked():
                if c_title[2:4] == "00":
                    # Month as title
                    c_title_date = (
                        c_title[0:2] + " - "
                        + self.comboBoxCopyMagIssue.currentText()[0:4])
                    self.show_debug_message("title_date:")
                    self.show_debug_message(c_title_date)
                else:
                    if re.match("\d{4,}", c_title) is not None:
                        # Date as title
                        c_title_date = (
                            c_title[2:4] + "." + c_title[0:2] + "."
                            + self.comboBoxCopyMagIssue.currentText()[0:4])
                    else:
                        # Title unchanged
                        c_title_date = c_title

            # Levels
            if self.checkBoxDaisyLevel.isChecked():
                # multible levels,
                # extract level-no from digit in filename
                # (1. digit after underline)
                self.show_debug_message(item[5:6])
                if self.checkBoxDaisyDateCalendar.isChecked():
                    f_out_file.write(
                        '<h' + item[5:6] + ' id="cnt_'
                        + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title_date
                        + '</a></h' + item[5:6] + '>' + '\r\n')
                else:
                    self.textEditDaisy.append(c_title)
                    f_out_file.write(
                        '<h' + item[5:6] + ' id="cnt_'
                        + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title
                        + '</a></h' + item[5:6] + '>' + '\r\n')
            else:
                if self.checkBoxDaisyDateCalendar.isChecked():
                    f_out_file.write(
                        '<h1 id="cnt_' + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title_date + '</a></h1>' + '\r\n')
                else:
                    self.textEditDaisy.append(c_title)
                    f_out_file.write(
                        '<h1 id="cnt_' + str(n_counter).zfill(4)
                        + '"><a href="' + str(n_counter).zfill(4)
                        + '.smil#txt_' + str(n_counter).zfill(4)
                        + '">' + c_title + '</a></h1>' + '\r\n')

        f_out_file.write("</body>" + '\r\n')
        f_out_file.write("</html>" + '\r\n')
        f_out_file.close
        self.textEditDaisy.append("<b>NCC-Datei geschrieben</b>")

    def write_master_smil(self, c_total_time, dir_audios):
        """write MasterSmil-page"""
        try:
            f_out_file = open(
                os.path.join(
                    str(self.lineEditDaisySource.text()), "master.smil"), 'w')
        except IOError as err:
            (errno, strerror) = err.args
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return

        self.textEditDaisy.append("<b>MasterSmil-Datei schreiben...</b>")
        f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
        f_out_file.write(
            '<!DOCTYPE smil PUBLIC "-//W3C//DTD SMIL 1.0//EN"'
            + ' "http://www.w3.org/TR/REC-smil/SMIL10.dtd">' + '\r\n')
        f_out_file.write('<smil>' + '\r\n')
        f_out_file.write('<head>' + '\r\n')
        f_out_file.write(
            '<meta name="dc:format" content="Daisy 2.02"/>' + '\r\n')
        f_out_file.write('<meta name="dc:identifier" content="'
                         + self.lineEditMetaRefOrig.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="dc:title" content="'
                         + self.lineEditMetaTitle.text() + '"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:generator"'
                         + ' content="KOM-IN-DaisyCreator"/>' + '\r\n')
        f_out_file.write('<meta name="ncc:format" content="Daisy 2.0"/>'
                         + '\r\n')
        f_out_file.write('<meta name="ncc:timeInThisSmil" content="'
                         + c_total_time + '" />' + '\r\n')

        f_out_file.write('<layout>' + '\r\n')
        f_out_file.write('<region id="txt-view" />' + '\r\n')
        f_out_file.write('</layout>' + '\r\n')
        f_out_file.write('</head>' + '\r\n')
        f_out_file.write('<body>' + '\r\n')

        # extract title
        self.show_debug_message("extract master smil title")
        z = 0
        for item in dir_audios:
            z += 1
            if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                for title in self.list_titles_from_files:
                    # self.show_debug_message(title)
                    if title[:4] == item[:4]:
                        if self.checkBoxDaisyLevel.isChecked():
                            c_title = title[7:]
                        else:
                            c_title = title[5:]
                        break
                    else:
                        file_name_separated = self.split_filename(item)
                        c_title = self.extract_title(file_name_separated)
            else:
                file_name_separated = self.split_filename(item)
                c_title = self.extract_title(file_name_separated)

            f_out_file.write(
                '<ref src="' + str(z).zfill(4) + '.smil" title="'
                + c_title + '" id="smil_' + str(z).zfill(4) + '"/>' + '\r\n')

        f_out_file.write('</body>' + '\r\n')
        f_out_file.write('</smil>' + '\r\n')
        f_out_file.close
        self.textEditDaisy.append("<b>Master-smil-Datei geschrieben</b>")

    def write_smil(self, l_total_elapsed_time, l_file_time, dir_audios):
        """write Smil-Pages"""
        self.textEditDaisy.append("<b>smil-Dateien schreiben...</b> ")
        z = 0
        for item in dir_audios:
            z += 1

            try:
                filename = str(z).zfill(4) + '.smil'
                f_out_file = open(os.path.join(
                    str(self.lineEditDaisySource.text()), filename), 'w')
            except IOError as err:
                (errno, strerror) = err.args
                self.show_debug_message(
                    "I/O error({0}): {1}".format(errno, strerror))
                return
            # else:
            self.textEditDaisy.append(
                str(z).zfill(4) + ".smil - File schreiben")
            # splitting
            if self.checkBoxDaisyIgnoreTitleDigits.isChecked():
                for title in self.list_titles_from_files:
                    # self.show_debug_message(title)
                    if title[:4] == item[:4]:
                        if self.checkBoxDaisyLevel.isChecked():
                            c_title = title[7:]
                        else:
                            c_title = title[5:]
                        break
                    else:
                        file_name_separated = self.split_filename(item)
                        c_title = self.extract_title(file_name_separated)
            else:
                file_name_separated = self.split_filename(item)
                c_title = self.extract_title(file_name_separated)

            f_out_file.write('<?xml version="1.0" encoding="utf-8"?>' + '\r\n')
            f_out_file.write(
                '<!DOCTYPE smil PUBLIC "-//W3C//DTD SMIL 1.0//EN"'
                + ' "http://www.w3.org/TR/REC-smil/SMIL10.dtd">' + '\r\n')
            f_out_file.write('<smil>' + '\r\n')
            f_out_file.write('<head>' + '\r\n')
            f_out_file.write(
                '<meta name="ncc:generator"'
                + ' content="KOM-IN-DaisyCreator"/>' + '\r\n')
            total_elapsed_time = timedelta(seconds=l_total_elapsed_time[z - 1])
            c_splitted_total_elapsed_time = str(total_elapsed_time).split(".")
            self.show_debug_message("c_splitted_total_elapsed_time: ")
            self.show_debug_message(c_splitted_total_elapsed_time)
            total_elapsed_time_hhmmss = (
                c_splitted_total_elapsed_time[0].zfill(8))
            if z == 1:
                # thirst item results in only one split
                total_elapsed_time_milli_micro = "000"
            else:
                if len(c_splitted_total_elapsed_time) > 1:
                    total_elapsed_time_milli_micro = (
                        c_splitted_total_elapsed_time[1][0:3])
                else:
                    total_elapsed_time_milli_micro = "000"

            f_out_file.write('<meta name="ncc:totalElapsedTime" content="'
                             + total_elapsed_time_hhmmss + "."
                             + total_elapsed_time_milli_micro + '"/>' + '\r\n')

            file_time = timedelta(seconds=l_file_time[z - 1])
            self.show_debug_message("file_time: " + str(file_time))
            file_time_splitted = str(file_time).split(".")
            file_time_hhmmss = file_time_splitted[0].zfill(8)
            # it's only one item in list when no milliseconds
            if len(file_time_splitted) > 1:
                if len(file_time_splitted[1]) >= 3:
                    file_time_milli_micro = file_time_splitted[1][0:3]
                elif len(file_time_splitted[1]) == 2:
                    file_time_milli_micro = file_time_splitted[1][0:2]
            else:
                file_time_milli_micro = "000"

            f_out_file.write(
                '<meta name="ncc:timeInThisSmil" content="'
                + file_time_hhmmss + "." + file_time_milli_micro
                + '" />' + '\r\n')
            f_out_file.write('<meta name="dc:format"'
                             + ' content="Daisy 2.02"/>' + '\r\n')
            f_out_file.write('<meta name="dc:identifier" content="'
                             + self.lineEditMetaRefOrig.text()
                             + '"/>' + '\r\n')
            f_out_file.write('<meta name="dc:title" content="' + c_title
                             + '"/>' + '\r\n')
            f_out_file.write('<layout>' + '\r\n')
            f_out_file.write('<region id="txt-view"/>' + '\r\n')
            f_out_file.write('</layout>' + '\r\n')
            f_out_file.write('</head>' + '\r\n')
            f_out_file.write('<body>' + '\r\n')
            l_file_time_seconds = str(l_file_time[z - 1]).split(".")

            f_out_file.write('<seq dur="' + l_file_time_seconds[0] + '.'
                             + file_time_milli_micro + 's">' + '\r\n')
            f_out_file.write('<par endsync="last">' + '\r\n')
            f_out_file.write('<text src="ncc.html#cnt_' + str(z).zfill(4)
                             + '" id="txt_' + str(z).zfill(4) + '" />'
                             + '\r\n')
            f_out_file.write('<seq>' + '\r\n')
            if file_time < timedelta(seconds=45):
                f_out_file.write('<audio src="' + item
                                 + '" clip-begin="npt=0.000s" clip-end="npt='
                                 + l_file_time_seconds[0] + '.'
                                 + file_time_milli_micro
                                 + 's" id="a_' + str(z).zfill(4)
                                 + '" />' + '\r\n')
            else:
                f_out_file.write(
                    '<audio src="' + item
                    + '" clip-begin="npt=0.000s" clip-end="npt='
                    + str(15) + '.' + file_time_milli_micro + 's" id="a_'
                    + str(z).zfill(4) + '" />' + '\r\n')
                zz = z + 1
                phrase_seconds = 15
                while phrase_seconds <= l_file_time[z - 1] - 15:
                    f_out_file.write(
                        '<audio src="' + item
                        + '" clip-begin="npt=' + str(phrase_seconds) + '.'
                        + file_time_milli_micro + 's" clip-end="npt='
                        + str(phrase_seconds + 15) + '.'
                        + file_time_milli_micro
                        + 's" id="a_' + str(zz).zfill(4) + '" />' + '\r\n')
                    phrase_seconds += 15
                    zz += 1
                f_out_file.write(
                    '<audio src="' + item
                    + '" clip-begin="npt=' + str(phrase_seconds) + '.'
                    + file_time_milli_micro + 's" clip-end="npt='
                    + l_file_time_seconds[0] + '.' + file_time_milli_micro
                    + 's" id="a_' + str(zz).zfill(4) + '" />' + '\r\n')

            f_out_file.write('</seq>' + '\r\n')
            f_out_file.write('</par>' + '\r\n')
            f_out_file.write('</seq>' + '\r\n')

            f_out_file.write('</body>' + '\r\n')
            f_out_file.write('</smil>' + '\r\n')
            f_out_file.close
        self.textEditDaisy.append("<b>smil-Dateien geschrieben:</b> " + str(z))

    def write_config_file(self):
        """write config file"""
        try:
            f_out_file = open("daisy_creator_mag.config", 'w')
        except IOError as err:
            (errno, strerror) = err.args
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return

        f_out_file.write('[Year]' + '\n')
        f_out_file.write(
            'business_year=' + self.lineEditConfigMagYear.text() + '\n')
        f_out_file.write('[Folders]' + '\n')
        f_out_file.write(
            'Source=' + self.lineEditConfigMag.text() + '\n')
        f_out_file.write(
            'Destination=' + self.lineEditConfigMagDest.text() + '\n')
        f_out_file.write(
            'Meta=' + self.lineEditConfigMagMeta.text() + '\n')
        f_out_file.write(
            'Issue_nr=' + self.lineEditConfigMagIssueNr.text() + '\n')
        f_out_file.write(
            'Intros=' + self.lineEditConfigMagIntro.text() + '\n')
        f_out_file.write(
            'Imprints=' + self.lineEditConfigMagImprints.text() + '\n')
        f_out_file.write(
            'Files_additionally='
            + self.lineEditConfigMagFileAdditionally.text()
            + '\n')
        f_out_file.write('\n')
        f_out_file.write('[Files]' + '\n')
        f_out_file.write(
            'file_prefix_issue_nr='
            + self.lineEditConfigMagPrefixIssueNr.text() + '\n')
        f_out_file.write(
            'file_prefix_meta='
            + self.lineEditConfigMagPrefixMeta.text() + '\n')
        f_out_file.write('\n')
        f_out_file.write('[Magazines]' + '\n')
        f_out_file.write(
            'Magazines=' + self.lineEditConfigMagDesc.text() + '\n')
        f_out_file.write('\n')
        f_out_file.write('[Programs]' + '\n')
        f_out_file.write(
            'DaisyPipeline_Package=' + self.lineEditConfigDP.text())
        f_out_file.write('\n')
        f_out_file.write(
            'DaisyPipeline=' + self.lineEditConfigValidator_1.text())
        f_out_file.write('\n')
        f_out_file.write(
            'DaisyPipeline_Script=' + self.lineEditConfigValidator_2.text())
        f_out_file.write('\n')
        f_out_file.write(
            'DaisyPipeline_Time=' + self.lineEditConfigValidator_3.text())
        f_out_file.write('\n')
        f_out_file.write(
            'DaisyPipeline_Report=' + self.lineEditConfigValidator_4.text())
        f_out_file.write('\n')
        f_out_file.close
        self.show_debug_message("write config file: daisy_creator_mag.config")
        self.textEditConfig.append("<b>Neue Config-Datei geschrieben:</b>")
        self.textEditConfig.append("daisy_creator_mag.config")
        self.progressBarConfig.setValue(20)

    def write_meta_file(self, path_file_source):
        """write config meta file"""
        try:
            f_out_file = open(path_file_source, 'w')
        except IOError as err:
            (errno, strerror) = err.args
            self.show_debug_message(
                "I/O error({0}): {1}".format(errno, strerror))
            return

        title_year = (self.lineEditMetaTitle.text()
                      [0:len(self.lineEditMetaTitle.text()) - 5]
                      + self.lineEditConfigMagYear.text() + "-")

        f_out_file.write('[Daisy_Meta]' + '\n')
        f_out_file.write(
            'Produzent=' + self.lineEditMetaProducer.text() + '\n')
        f_out_file.write(
            'Autor=' + self.lineEditMetaAutor.text() + '\n')
        f_out_file.write(
            'Titel=' + title_year + '\n')
        f_out_file.write(
            'Edition=' + self.lineEditMetaEdition.text() + '\n')
        f_out_file.write(
            'Sprecher=' + self.lineEditMetaNarrator.text() + '\n')
        f_out_file.write(
            'Stichworte=' + self.lineEditMetaKeywords.text()
            + '\n')
        f_out_file.write(
            'ISBN/Ref-Nr.Original='
            + self.lineEditMetaRefOrig.text() + '\n')
        f_out_file.write('Verlag=' + self.lineEditMetaPublisher.text() + '\n')
        f_out_file.write('Jahr=' + self.lineEditConfigMagYear.text())
        f_out_file.write('\n')
        f_out_file.close
        self.show_debug_message("write meta file: " + path_file_source)

    def split_filename(self, item):
        """split filename into list"""
        if self.comboBoxDaisyTrenner.currentText() == "Ausgabe-Nr.":
            file_name_separated = (
                item.split(self.comboBoxCopyMagIssue.currentText() + "_"))
            # file_name_separated = item.split("_", 2)
        # self.show_debug_message(file_name_separated)
        # self.show_debug_message(len(file_name_separated))
        return file_name_separated

    def extract_title(self, file_name_separated):
        """extract title """
        # last piece
        item_left = file_name_separated[len(file_name_separated) - 1]
        # now split file-extention
        item_title = item_left.split(".mp3")
        c_title = re.sub("_", " ", item_title[0])
        return c_title

    def show_dialog_critical(self, error_message):
        """show messagebox warning"""
        QtWidgets.QMessageBox.critical(self, "Achtung", error_message)

    def show_dialog_question(self, display_message):
        """show messagebox question"""
        msgBox = QtWidgets.QMessageBox()
        msgBox.setIcon(QtWidgets.QMessageBox.Question)
        msgBox.setInformativeText(display_message)
        msgBox.setText("Achtung!")
        msgBox.setStandardButtons(
            QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Cancel)
        msgBox.setDefaultButton(QtWidgets.QMessageBox.Save)
        ret = msgBox.exec_()
        action = 0
        if ret == QtWidgets.QMessageBox.Save:
            # Save was clicked
            debugMessage = "Save was clicked"
            self.show_debug_message(debugMessage)
            action = 1
        elif ret == QtWidgets.QMessageBox.Cancel:
            # cancel was clicked
            debugMessage = "Cancel was clicked"
            self.show_debug_message(debugMessage)
        return action

    def show_debug_message(self, debugMessage):
        """show messagebox """
        if self.app_debug_mod == "yes":
            print(debugMessage)

    def action_tab_select(self, n):
        """set focus on tab n"""
        self.tabWidget.setCurrentIndex(n)

    def action_quit(self):
        """exit the application"""
        QtWidgets.qApp.quit()

    def make_config_dirs(self):
        """make config dirs"""
        self.show_debug_message("make config dirs:")
        self.textEditConfig.append("<b>Neue Config-Ordner:</b>")
        config_dirs = []
        config_dirs.append(self.lineEditConfigMag.text())
        config_dirs.append(self.lineEditConfigMagDest.text())
        config_dirs.append(self.lineEditConfigMagMeta.text())
        config_dirs.append(self.lineEditConfigMagIssueNr.text())
        config_dirs.append(self.lineEditConfigMagIntro.text())
        config_dirs.append(self.lineEditConfigMagImprints.text())
        n = 0
        for config_dir in config_dirs:
            try:
                os.makedirs(str(config_dir))
                self.show_debug_message(config_dir)
            except OSError:
                if not os.path.isdir(config_dir):
                    self.show_dialog_critical(
                        "Fehler beim erstellen von: " + config_dir)
                else:
                    self.show_debug_message("dir always exists:")
                    self.show_debug_message(config_dir)
            self.textEditConfig.append(config_dir)
            n += 1
        self.progressBarConfig.setValue(30)

    def report_progress(self, message):
        self.show_debug_message(message)
        self.textEditConfig.append(message)

    def main(self):
        """main function"""

        self.show_debug_message("let's rock")
        config_file_ok = self.read_config()
        self.progressBarCopy.setValue(0)
        self.progressBarDaisy.setValue(0)
        self.progressBarConfig.setValue(0)
        # mag in Combo
        for item in self.app_mag_items:
            self.comboBoxCopyMag.addItem(item)
        # Combo-items: numbers according to years
        year_prev = str(datetime.datetime.now().year - 1)
        year_current = str(datetime.datetime.now().year)
        year_next = str(datetime.datetime.now().year + 1)
        for item in self.app_issue_items_prev:
            self.comboBoxCopyMagIssue.addItem(year_prev + "_" + item)
        for item in self.app_issue_items_current:
            self.comboBoxCopyMagIssue.addItem(year_current + "_" + item)
        for item in self.app_issue_items_next:
            self.comboBoxCopyMagIssue.addItem(year_next + "_" + item)
        # Combo-items: string for splitting author and title in filenames
        self.comboBoxDaisyTrenner.addItem("Ausgabe-Nr.")
        self.comboBoxDaisyTrenner.addItem(year_prev)
        self.comboBoxDaisyTrenner.addItem(year_current)
        self.comboBoxDaisyTrenner.addItem(year_next)
        self.comboBoxDaisyTrenner.addItem("-")
        self.comboBoxDaisyTrenner.addItem("_")
        self.comboBoxDaisyTrenner.addItem("_-_")
        self.comboBoxPrefBitrate.addItem("64")
        self.comboBoxPrefBitrate.addItem("80")
        self.comboBoxPrefBitrate.addItem("96")
        self.comboBoxPrefBitrate.addItem("128")
        self.comboBoxPrefBitrate.setCurrentIndex(1)
        # defaults for checkboxes
        self.checkBoxCopyMagIntro.setChecked(True)
        self.checkBoxCopyMagIssueAnnouncement.setChecked(True)
        self.checkBoxCopyImprint.setChecked(True)
        self.checkBoxCopyID3Change.setChecked(True)
        self.checkBoxCopyBitrateChange.setChecked(True)
        self.checkBoxCopyFileNameShort.setChecked(True)

        # Help-Text
        self.read_help()
        self.read_user_help()
        self.show()

        if config_file_ok is False:
            # don't waste time with further checks
            return

        # checking installed deb packages takes a long time
        # therefor wie use a thread to run this in background
        # thanx to
        # https://realpython.com/python-pyqt-qthread/
        # Step 2: Create a QThread object
        self.thread = QtCore.QThread()
        # Step 3: Create a worker object
        self.worker = Worker()
        self.worker.packages_to_check.append("lame")
        self.worker.packages_to_check.append(self.lineEditConfigDP.text())
        # self.show_debug_message(self.worker.packages_to_check)
        # Step 4: Move worker to the thread
        self.worker.moveToThread(self.thread)
        # Step 5: Connect signals and slots
        self.thread.started.connect(self.worker.run_packages_to_check)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.progress.connect(self.report_progress)
        # Step 6: Start the thread
        self.thread.start()

        # check for python3-html2text and python3-mutagen
        self.check_modules("html2text")
        self.check_modules("mutagen")
        self.check_modules("qdarktheme")


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    # Apply dark theme to Qt application
    try:
        import qdarktheme
        app.setStyleSheet(qdarktheme.load_stylesheet())
    except Exception as e:
        print("Fehler beim Laden von qdarktheme: %s" % str(e))

    dyc = DaisyCopy()
    dyc.main()
    app.exec_()
